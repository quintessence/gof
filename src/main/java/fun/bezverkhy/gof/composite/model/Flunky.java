package fun.bezverkhy.gof.composite.model;

public class Flunky implements Employee {

  private String name;
  private Integer age;
  private Integer salary;

  public Flunky(String name, Integer age, Integer salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public Integer getAge() {
    return age;
  }

  @Override
  public Integer getSalary() {
    return salary;
  }

  @Override
  public void payRespects() {
    System.out.println(String.format("%s respects you!", name));
  }

  @Override
  public String toString() {
    return name;
  }
}

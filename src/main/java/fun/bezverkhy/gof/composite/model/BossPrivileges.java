package fun.bezverkhy.gof.composite.model;

public interface BossPrivileges {

  void hire(Employee employee);

  void fire(Employee employee);

  Employee get(int index);
}

package fun.bezverkhy.gof.composite.model;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class Boss implements Employee, BossPrivileges {

  private String name;
  private Integer age;
  private Integer salary;

  private List<Employee> employees = new ArrayList<>();

  public Boss(String name, Integer age, Integer salary) {
    this.name = name;
    this.age = age;
    this.salary = salary;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public Integer getAge() {
    return age;
  }

  @Override
  public Integer getSalary() {
    return salary;
  }

  @Override
  public void payRespects() {
    System.out.println(String.format("You've just got respected by %s!", name));
    employees.forEach(Employee::payRespects);
  }

  @Override
  public void hire(Employee employee) {
    employees.add(employee);
  }

  @Override
  public void fire(Employee employee) {
    employees.remove(employee);
  }

  @Override
  public Employee get(int index) {
    return employees.get(index);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", Boss.class.getSimpleName() + "[", "]")
        .add("name='" + name + "'")
        .add("age=" + age)
        .add("salary=" + salary)
        .add("employees=" + employees)
        .toString();
  }
}

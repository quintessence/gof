package fun.bezverkhy.gof.composite.model;

public interface Employee {

  String getName();

  Integer getAge();

  Integer getSalary();

  void payRespects();
}

package fun.bezverkhy.gof.composite;

import fun.bezverkhy.gof.composite.model.Boss;
import fun.bezverkhy.gof.composite.model.Employee;
import fun.bezverkhy.gof.composite.model.Flunky;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CompositeDemo {

  public static void main(String[] args) {
    Boss sashaBelyi = new Boss("Sasha Belyi", 38, 150000);

    Flunky kosmos = new Flunky("Kosmos", 39, 120000);
    Flunky pchela = new Flunky("Pchela", 35, 90000);
    Flunky fil = new Flunky("Fil", 37, 100000);

    sashaBelyi.hire(kosmos);
    sashaBelyi.hire(pchela);
    sashaBelyi.hire(fil);

    System.out.println(sashaBelyi);
    System.out.println();

    List<Employee> brigada = new ArrayList<>(Arrays.asList(sashaBelyi, kosmos, pchela, fil));

    System.out.println(brigada);
    System.out.println();

    sashaBelyi.payRespects();
  }
}

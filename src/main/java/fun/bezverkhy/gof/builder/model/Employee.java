package fun.bezverkhy.gof.builder.model;

import java.util.Objects;
import java.util.StringJoiner;

public class Employee {

  private Long id;
  private String name;
  private Integer age;
  private Integer salary;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  public Integer getSalary() {
    return salary;
  }

  public void setSalary(Integer salary) {
    this.salary = salary;
  }

  @Override
  public boolean equals(Object object) {
    if (this == object) {
      return true;
    }
    if (object == null || getClass() != object.getClass()) {
      return false;
    }
    Employee employee = (Employee) object;
    return Objects.equals(name, employee.name) &&
        Objects.equals(age, employee.age) &&
        Objects.equals(salary, employee.salary);
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, age, salary);
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", Employee.class.getSimpleName() + "[", "]")
        .add("id=" + id)
        .add("name='" + name + "'")
        .add("age=" + age)
        .add("salary=" + salary)
        .toString();
  }
}

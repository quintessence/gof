package fun.bezverkhy.gof.builder.extractor.impl;

import fun.bezverkhy.gof.builder.extractor.Extractor;
import fun.bezverkhy.gof.builder.model.Employee;
import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeExtractor implements Extractor<Employee, ResultSet> {

  @Override
  public Employee extractFrom(ResultSet resultSet) throws SQLException {
    Employee employee = new Employee();

    int index = 1;
    employee.setId(resultSet.getLong(index++));
    employee.setName(resultSet.getString(index++));
    employee.setAge(resultSet.getInt(index++));
    employee.setSalary(resultSet.getInt(index));

    return employee;
  }
}

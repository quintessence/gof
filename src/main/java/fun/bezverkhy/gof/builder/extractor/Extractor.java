package fun.bezverkhy.gof.builder.extractor;

import java.sql.SQLException;

public interface Extractor<E, T> {

  E extractFrom(T source) throws SQLException;
}


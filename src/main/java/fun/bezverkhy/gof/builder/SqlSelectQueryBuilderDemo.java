package fun.bezverkhy.gof.builder;

import fun.bezverkhy.gof.builder.extractor.Extractor;
import fun.bezverkhy.gof.builder.extractor.impl.EmployeeExtractor;
import fun.bezverkhy.gof.builder.model.Employee;
import fun.bezverkhy.gof.builder.util.SqlSelectQueryBuilder;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SqlSelectQueryBuilderDemo {

  public static void main(String[] args) throws SQLException {
    Connection connection =
        DriverManager.getConnection("jdbc:h2:tcp://localhost/~/test", "sa", "");

    List<Employee> employees = new ArrayList<>();

    try (PreparedStatement pstmt = buildQuery(connection)) {
      try (ResultSet rs = pstmt.executeQuery()) {
        Extractor<Employee, ResultSet> extractor = new EmployeeExtractor();
        while (rs.next()) {
          employees.add(extractor.extractFrom(rs));
        }
      }
    }

    System.out.println(employees);
  }

  private static PreparedStatement buildQuery(Connection connection)
      throws SQLException {
    return SqlSelectQueryBuilder.from("*", "test.employees")
        .equals("name", "Onotole")
        .more("age", "35")
        .less("age", "50")
        .order("age", "DESC")
        .limit(1, 2)
        .prepareStatement(connection);
  }
}

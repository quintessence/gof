package fun.bezverkhy.gof.builder.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class SqlSelectQueryBuilder {

  private static final String WHERE = " WHERE ";
  private static final String AND = " AND ";

  private static final String LIKE = " LIKE ";
  private static final String EQUALS = "=";
  private static final String MORE = ">";
  private static final String LESS = "<";

  private static final String ORDER_BY = " ORDER BY ";
  private static final String LIMIT = " LIMIT ";
  private static final Character PARAM = '?';

  private static final Map<String, String> EQUALS_CONDITIONS = new HashMap<>();
  private static final Map<String, String> LIKE_CONDITIONS = new HashMap<>();
  private static final Map<String, String> MORE_CONDITIONS = new HashMap<>();
  private static final Map<String, String> LESS_CONDITIONS = new HashMap<>();

  private boolean whereAppeared = false;

  private String select;
  private String orderBy = "";
  private String limit = "";

  private SqlSelectQueryBuilder(String field, String table) {
    select = String.format("SELECT %s FROM %s", field, table);
  }

  public static SqlSelectQueryBuilder from(String field, String table) {
    return new SqlSelectQueryBuilder(field, table);
  }

  public SqlSelectQueryBuilder equals(String field, String value) {
    return addWhereCondition(field, value, EQUALS_CONDITIONS);
  }

  public SqlSelectQueryBuilder like(String field, String value) {
    return addWhereCondition(field, value, LIKE_CONDITIONS);
  }

  public SqlSelectQueryBuilder more(String field, String value) {
    return addWhereCondition(field, value, MORE_CONDITIONS);
  }

  public SqlSelectQueryBuilder less(String field, String value) {
    return addWhereCondition(field, value, LESS_CONDITIONS);
  }

  public SqlSelectQueryBuilder order(String field, String order) {
    if (field != null && order != null) {
      orderBy = ORDER_BY + field + ' ' + order;
    }
    return this;
  }

  public SqlSelectQueryBuilder limit(Integer offset, Integer count) {
    limit = LIMIT + offset + ',' + count;
    return this;
  }

  public PreparedStatement prepareStatement(Connection connection) throws SQLException {
    StringBuilder query = new StringBuilder();

    query.append(select);

    addConditions(query, EQUALS_CONDITIONS, EQUALS);
    addConditions(query, LIKE_CONDITIONS, LIKE);
    addConditions(query, MORE_CONDITIONS, MORE);
    addConditions(query, LESS_CONDITIONS, LESS);

    query.append(orderBy);
    query.append(limit);

    PreparedStatement pstmt = connection.prepareStatement(query.toString());

    int index = 1;
    for (String value : EQUALS_CONDITIONS.values()) {
      pstmt.setObject(index++, value);
    }
    for (String value : LIKE_CONDITIONS.values()) {
      pstmt.setObject(index++, '%' + value + '%');
    }
    for (String value : MORE_CONDITIONS.values()) {
      pstmt.setObject(index++, Integer.parseInt(value));
    }
    for (String value : LESS_CONDITIONS.values()) {
      pstmt.setObject(index++, Integer.parseInt(value));
    }

    return pstmt;
  }

  private SqlSelectQueryBuilder addWhereCondition(String field, String value,
      Map<String, String> conditions) {
    if (value != null) {
      conditions.put(field, value);
    } else {
      conditions.remove(field, value);
    }
    return this;
  }

  private void addConditions(StringBuilder query, Map<String, String> conditions,
      String operation) {
    conditions.keySet()
        .forEach(field -> query
            .append(getConditionPrefix())
            .append(field)
            .append(operation)
            .append(PARAM));
  }

  private String getConditionPrefix() {
    if (whereAppeared) {
      return AND;
    } else {
      whereAppeared = true;
      return WHERE;
    }
  }
}

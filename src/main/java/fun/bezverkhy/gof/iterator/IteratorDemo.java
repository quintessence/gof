package fun.bezverkhy.gof.iterator;

import fun.bezverkhy.gof.iterator.util.COWMyList;
import java.util.Iterator;

public class IteratorDemo {

  public static void main(String[] args) {
    // not {java.util.List} for faster navigation
    COWMyList<Integer> list = new COWMyList<>();

    list.add(3);
    list.add(8);
    list.add(5);

    Iterator<Integer> it = list.iterator();
    int index = 1;
    while (it.hasNext()) {
      list.add(7);
      System.out.println(String.format("%d) %d;", index++, it.next()));
    }
    System.out.println();

    System.out.println(list);
  }
}

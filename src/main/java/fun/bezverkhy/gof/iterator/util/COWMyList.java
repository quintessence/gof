package fun.bezverkhy.gof.iterator.util;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Observable;
import java.util.Observer;

/**
 * Array-based implementation of the {@link List} that extends default iterator. Inner array
 * automatically expands when needed. The iterator is guaranteed to not throw {@link
 * java.util.ConcurrentModificationException}. Allows to modify list while iterating.
 *
 * @param <E> Allows to use only elements with type E.
 */
public class COWMyList<E> extends Observable implements List<E> {

  private static final int INITIAL_CAPACITY = 10;
  private static final int INCREASE_COEFFICIENT = 2;

  private Object[] innerArray;
  private int size = 0;

  /**
   * Default constructor for {@link COWMyList}. Initializes innerArray with size of INITIAL_CAPACITY
   * constant.
   */
  public COWMyList() {
    this.innerArray = new Object[INITIAL_CAPACITY];
  }

  /**
   * Ensure the capacity of inner array when new elements need to be added.
   *
   * @param newSize Count of elements in innerArray after modification.
   */
  private void ensureCapacity(int newSize) {
    while (newSize >= innerArray.length) {
      innerArray = Arrays.copyOf(innerArray, innerArray.length * INCREASE_COEFFICIENT);
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean add(E element) {
    setChanged();
    notifyObservers();
    ensureCapacity(size());
    innerArray[size++] = element;
    return true;
  }

  /**
   * Checks if index is valid and we are in range.
   *
   * @param index index to be checked
   */
  private void indexInRangeCheck(int index) {
    if (index < 0 || index >= size()) {
      throw new IndexOutOfBoundsException();
    }
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void add(int index, E element) {
    indexInRangeCheck(index);
    setChanged();
    notifyObservers();
    ensureCapacity(size());
    System.arraycopy(innerArray, index, innerArray, index + 1, size() - index);
    innerArray[index] = element;
    size++;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean addAll(Collection<? extends E> collection) {
    setChanged();
    notifyObservers();
    ensureCapacity(this.size() + collection.size());
    System.arraycopy(collection.toArray(), 0, innerArray, size(), collection.size());
    this.size = this.size() + collection.size();
    return true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean addAll(int index, Collection<? extends E> collection) {
    indexInRangeCheck(index);
    setChanged();
    notifyObservers();
    ensureCapacity(this.size() + collection.size());
    System.arraycopy(innerArray, index, innerArray, index + collection.size(), size() - index);
    System.arraycopy(collection.toArray(), 0, innerArray, index, collection.size());
    this.size = this.size() + collection.size();
    return true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public void clear() {
    setChanged();
    notifyObservers();
    this.innerArray = new Object[INITIAL_CAPACITY];
    size = 0;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean contains(Object element) {
    return indexOf(element) != -1;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean containsAll(Collection<?> collection) {
    for (Object element : collection) {
      if (!contains(element)) {
        return false;
      }
    }
    return true;
  }

  /**
   * {@inheritDoc}
   */
  @SuppressWarnings("unchecked")
  @Override
  public E get(int index) {
    indexInRangeCheck(index);
    return (E) innerArray[index];
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int indexOf(Object element) {
    int index = -1;
    if (element == null) {
      for (int i = 0; i < size(); i++) {
        if (innerArray[i] == null) {
          index = i;
          break;
        }
      }
    } else {
      for (int i = 0; i < size(); i++) {
        if (element.equals(innerArray[i])) {
          index = i;
          break;
        }
      }
    }
    return index;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean isEmpty() {
    return size() == 0;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Iterator<E> iterator() {
    return new IteratorImpl();
  }

  /**
   * Default Iterator implementation. Allows to change list while iterations, guaranteed to not
   * throw {@link java.util.ConcurrentModificationException}.
   */
  private class IteratorImpl implements Iterator<E>, Observer {

    private int index = -1; // start position is -1
    private Object[] snapshot;
    private boolean snapshotSaved = false;

    IteratorImpl() {
      addObserver(this);
    }

    @Override
    public boolean hasNext() {
      if (snapshot != null) {
        if (index < snapshot.length - 1) {
          return true;
        }
      } else {
        if (index < size() - 1) {
          return true;
        }
      }
      deleteObserver(this);
      return false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public E next() {
      if (hasNext()) {
        if (snapshot != null) {
          return (E) snapshot[++index];
        }
        return (E) innerArray[++index];
      }
      throw new NoSuchElementException();
    }

    @Override
    public void update(Observable observable, Object object) {
      if (!snapshotSaved) {
        snapshot = Arrays.copyOf(innerArray, size());
        snapshotSaved = true;
      }
    }

  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int lastIndexOf(Object element) {
    int index = -1;
    if (element == null) {
      for (int i = size() - 1; i >= 0; i--) {
        if (innerArray[i] == null) {
          index = i;
          break;
        }
      }
    } else {
      for (int i = size() - 1; i >= 0; i--) {
        if (element.equals(innerArray[i])) {
          index = i;
          break;
        }
      }
    }
    return index;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ListIterator<E> listIterator() {
    throw new UnsupportedOperationException("Not supported!");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public ListIterator<E> listIterator(int arg0) {
    throw new UnsupportedOperationException("Not supported!");
  }

  /**
   * Removes element by its index.
   *
   * @param index Index of element we want to remove from the list.
   */
  private void removeElementByIndex(int index) {
    setChanged();
    notifyObservers();
    System.arraycopy(innerArray, index + 1, innerArray, index, size() - index - 1);
    innerArray[--size] = null;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean remove(Object element) {
    boolean result = false;
    if (element == null) {
      for (int i = 0; i < size(); i++) {
        if (innerArray[i] == null) {
          removeElementByIndex(i);
          result = true;
          break;
        }
      }
    } else {
      for (int i = 0; i < size(); i++) {
        if (element.equals(innerArray[i])) {
          removeElementByIndex(i);
          result = true;
          break;
        }
      }
    }
    return result;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public E remove(int index) {
    indexInRangeCheck(index);
    E deletedValue = this.get(index);
    removeElementByIndex(index);
    return deletedValue;
  }

  /**
   * @param collection Collection of elements we want to remove or retain.
   * @param condition Allow to choice: remove or retain elements. Removes when true, retains
   * otherwise.
   * @return true if the method finished successfully.
   */
  private boolean removeOrRetainAll(Collection<?> collection, boolean condition) {
    for (int i = 0; i < size(); i++) {
      if (collection.contains(this.get(i)) == condition) {
        this.remove(i--);
      }
    }
    return true;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean removeAll(Collection<?> collection) {
    return removeOrRetainAll(collection, true);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public boolean retainAll(Collection<?> collection) {
    return removeOrRetainAll(collection, false);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public E set(int index, E element) {
    indexInRangeCheck(index);
    setChanged();
    notifyObservers();
    E oldValue = this.get(index);
    this.innerArray[index] = element;
    return oldValue;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public int size() {
    return size;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public List<E> subList(int arg0, int arg1) {
    throw new UnsupportedOperationException("Not supported!");
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public Object[] toArray() {
    return Arrays.copyOf(innerArray, size());
  }

  /**
   * {@inheritDoc}
   */
  @SuppressWarnings("unchecked")
  @Override
  public <T> T[] toArray(T[] array) {
    if (array.length < size()) {
      array = (T[]) this.toArray();
    } else {
      System.arraycopy(this.toArray(), 0, array, 0, size());
      if (array.length > size()) {
        array[size()] = null;
      }
    }
    return array;
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    return Arrays.toString(Arrays.copyOf(innerArray, size()));
  }
}

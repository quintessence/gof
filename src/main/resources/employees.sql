DROP TABLE IF EXISTS test.employees;

CREATE TABLE test.employees
(
  ID         BIGINT           PRIMARY KEY,
  NAME       VARCHAR(10),
  AGE        INT,
  SALARY     INT
);

INSERT INTO test.employees VALUES(1, 'Vasya', 28, 5000);
INSERT INTO test.employees VALUES(2, 'Onotole', 24, 3000);
INSERT INTO test.employees VALUES(3, 'Onotole', 43, 14000);
INSERT INTO test.employees VALUES(4, 'Onotole', 38, 23000);
INSERT INTO test.employees VALUES(5, 'Onotole', 47, 16000);
